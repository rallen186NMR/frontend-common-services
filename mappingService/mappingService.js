
/************************Create map, A,B markers****************************/
      //popup variables
      var popup, Popup;
      
      // Create a map and center it on Austin.
      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            mapTypeControl: false,
            center: {lat: 30.267153, lng: -97.7430608},
            zoom: 13
        });
        
        //Create marker and bind it to the map
        var marker = new google.maps.Marker({
            map: map,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(30.267153,-97.7430608)
        });
        marker.addListener('click', toggleBounce);
        new AutocompleteDirectionsHandler(map);
        
        //Create marker
        marker1=new google.maps.Marker({map:map,icon:"http://maps.google.com/mapfiles/ms/micons/blue.png"});
        //Create popup and bind to map for Autonomous Vehicle Supply location
        Popup = createPopupClass();
        popup = new Popup(
        //Autonomous Vehicle Supply    
        new google.maps.LatLng(30.267153, -97.7430608),
        document.getElementById('content'));
        popup.setMap(map);

/*********************Popups And Markers Animation**************************/        
        //Create popup and bind to map for Farm location
        Popup = createPopupClass();
        popup = new Popup(
        //Farm waypoint location
        new google.maps.LatLng(30.233516, -97.748728),
        document.getElementById('wayPnt'));
        popup.setMap(map);
      }
      
        //Toggle the marker with animation
        function toggleBounce() {
          if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
          } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
          }
        }
      
      /*popup function and calls
      * Returns the Popup class.
      * Unfortunately, the Popup class can only be defined after
      * google.maps.OverlayView is defined, when the Maps API is loaded.
      * This function should be called by initMap.*/
      function createPopupClass() 
      {
        
        /**A customized popup on the map.
        * @param {!google.maps.LatLng} position
        * @param {!Element} content The bubble div.
        * @constructor
        * @extends {google.maps.OverlayView}*/
        function Popup(position, content) {
            this.position = position;

            content.classList.add('popup-bubble');

            // This zero-height div is positioned at the bottom of the bubble.
            var bubbleAnchor = document.createElement('div');
            bubbleAnchor.classList.add('popup-bubble-anchor');
            bubbleAnchor.appendChild(content);

            // This zero-height div is positioned at the bottom of the tip.
            this.containerDiv = document.createElement('div');
            this.containerDiv.classList.add('popup-container');
            this.containerDiv.appendChild(bubbleAnchor);

            // Optionally stop clicks, etc., from bubbling up to the map.
            google.maps.OverlayView.preventMapHitsAndGesturesFrom(this.containerDiv);
        }//End function Popup()
    
        // ES5 magic to extend google.maps.OverlayView.
        Popup.prototype = Object.create(google.maps.OverlayView.prototype);

        /** Called when the popup is added to the map. */
        Popup.prototype.onAdd = function() 
        {
            this.getPanes().floatPane.appendChild(this.containerDiv);
        };

        /* Called when the popup is removed from the map. */
        Popup.prototype.onRemove = function() 
        {
            if (this.containerDiv.parentElement) 
            {
                this.containerDiv.parentElement.removeChild(this.containerDiv);
            }
        };

        /* Called each frame when the popup needs to draw itself. */
        Popup.prototype.draw = function() 
        {
            var divPosition = this.getProjection().fromLatLngToDivPixel(this.position);

            // Hide the popup when it is far out of view.
            var display =
            Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ?
            'block' :
            'none';

            if (display === 'block') 
            {
                this.containerDiv.style.left = divPosition.x + 'px';
                this.containerDiv.style.top = divPosition.y + 'px';
            }
            if (this.containerDiv.style.display !== display) 
            {
                this.containerDiv.style.display = display;
            }
        };
        return Popup;
      }//end createPopupClass()

/*******************AutoComplete and Direction Handlers *********************/
      /*AutoComplete Handler */
      function AutocompleteDirectionsHandler(map) {
        this.map = map;
        //this.originPlaceId = null;
        this.destinationPlaceId = null;
        this.travelMode = 'DRIVING';
        //var originInput = document.getElementById('origin-input');
        var destinationInput = document.getElementById('destination-input');
        //var modeSelector = document.getElementById('mode-selector');
        this.directionsService = new google.maps.DirectionsService;
        this.directionsDisplay = new google.maps.DirectionsRenderer;
        this.directionsDisplay.setMap(map);

        var destinationAutocomplete = new google.maps.places.Autocomplete(
            //get destination input lat long if we comment out placeIdOnly
            destinationInput /*, {placeIdOnly: true}*/ ); 

        //Transportation is limited to 'Driving' for WeGo
        /*
        this.setupClickListener('changemode-walking', 'WALKING');
        this.setupClickListener('changemode-transit', 'TRANSIT');
        this.setupClickListener('changemode-driving', 'DRIVING');
        */

        /**Wego only needs destination location for now**/
        //this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
        this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');

        /********This pushes controls to top left of map container************/ 
        //this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(originInput);
        //this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(destinationInput);
        //this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(modeSelector);
      }

      /* Sets a listener on a radio button to change the filter type on Places
       Autocomplete.*/
      AutocompleteDirectionsHandler.prototype.setupClickListener = function(id, mode) 
      {
        var radioButton = document.getElementById(id);
        var me = this;
        radioButton.addEventListener('click', function() 
        {
          me.travelMode = mode;
          me.route();
        });
      };
      
      /* Sets a listener type on Places
         Autocomplete.*/
      AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(autocomplete, mode) 
      {
        var me = this;
        autocomplete.bindTo('bounds', this.map);
        autocomplete.addListener('place_changed', function() 
        {
          var place = autocomplete.getPlace();
          if (!place.place_id) 
          {
            window.alert("Please select an option from the dropdown list.");
            return;
          }
          me.destinationPlaceId = place.place_id;
          
          /*This passes back the lat long to html Id*/
          //get the latitude and longitude from the place object
          //document.getElementById("dest_latitude").value= place.geometry.location.lat();
         
         /*This passes back the lat long to html dest_latlong Id */ 
         document.getElementById("dest_latlong").value= place.geometry.location;
         me.route();
        });
      };
      
      /* Route the directions and pass the response.
         Added waypoint for farm to customer destination.
         Need to delete waypoints,location frm below for Rent-A-Swag */
      AutocompleteDirectionsHandler.prototype.route = function() 
      {
        if (!this.destinationPlaceId) 
        {
          return;
        }
        var me = this;

        //Origin is location for atonomous vehicle service provider. 
        //Destination is address for customer
        this.directionsService.route(
        {
            origin: new google.maps.LatLng(30.267153, -97.7430608),
            destination: {'placeId': this.destinationPlaceId},
            //Waypoints location
            waypoints: [{stopover: false,
            location: new google.maps.LatLng(30.233516, -97.748728)}],
              travelMode: this.travelMode
        }, 
        function(response, status) 
        {
            if (status === 'OK') 
            {
                me.directionsDisplay.setDirections(response);
                //Get steps
                var leg = response.routes[0].legs[0];
                
                //distance AND duration to alert user;
                var dist = leg.distance.text;
                var duration = leg.duration.text;
                
                //show the distance at some div with pop up alert!
                window.alert('Currently in route to your destination. We will make a quick stop at WeGo HQ for your items. Your vehicle is currently ' + dist + 'les and ' + duration + ' away from your location.');
            } 
            else 
            {
                window.alert('Directions request failed due to ' + status);
            }
        });
      };

/***************************HTTP Get ************************************/ 
    /******************Request worked on POSTMAN***************/
      //Server Side API
      /*
	  TODO
      function getCommuteTime() {
        //window.alert('Your vehicle is on the way, ETA:  ');
        //Put your API call here with the parameters passed into it
        var queryURL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + origin_Commute + "&destinations=" + origin_Commute + "&key=AIzaSyAyfjcxAXy8Iq-GlT1ltwGnIomOVc255-I";

        //Ajax retrieves data from the API
        $.ajax({
        url: queryURL,
        method: "GET"
      }).then(function(response) {

      //Navigate through the given object to the data you want (in this case, commute time)
        var commuteTime = response.rows[0].elements[0].duration.text;
        //window.alert('Your vehicle is on the way, ETA:  ' + commuteTime);
    
        console.log(commuteTime) });
      } 
      */

/*********************************End JS************************************/


/*****************MAPPING SERVICE TESTING PURPOSES HTML************************/
    /*****Some HTML BTNS & DIVS IN HERE NEEDED FOR INTEGRATION******/
/*
<!DOCTYPE html>
<html>
  <head>
    <title>WeGo TAAS</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <!--CSS-->
    <link rel='stylesheet' type='text/css' href='./css/mappingService.css'>
    <!--Ajax-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!--Google Api key, Must include to render APIs-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAyfjcxAXy8Iq-GlT1ltwGnIomOVc255-I&libraries=places&callback=initMap"async defer>
    </script>
    <script type="text/javascript" src="./js/mappingService.js"></script>

  </head>
      <body class='body'> 
        
            <div class='heading'>
            <div class='row'>
            <div class='col-xs-12' >
            <h2 class= 'myHeading'> WeGo </h2> 
            </div> <!--col-xs-12 --> 
            </div><!-- /row --> 
            </div> <!-- /heading <div class='container'>  -->
    <div class="container">
        
	<!------------Lat Long from js for later use--------------------------->
		<!--Get latlong from destination-input placeId Object script>-->
		<!--<input type="hidden" id="dest_latitude" name="dest_latitude"  />-->
		<!-- destination latlong here-->
        <input type="hidden" id="dest_longitude" name="dest_longitude"  />
        
	<!------------Map searchbox for shipping destination--------------->
        <div style=" margin-top:100px;background: #F0F0F0; width: 100%; height:100px;">
            		<h3>Shipping Address:</h3>
            <input id="destination-input" class="controls" type="text"
            placeholder="Enter a destination location">
        </div>
        
        <!--TODO-->
        <div id="animateBtn"> 
            <button onclick="getCommuteTime();">Animate Route!</button>
        </div>
        
    <!-------------------Map and Animation popups-------------------->
        <!--Render Map Object--> 
        <div id="map"></div>
        
        <!--Pop-up Content for Wego Vehicle location and waypoint-->
        <div id="content">
            WeGo Veh Loc
        </div>
        <div id="wayPnt">
            WeGo HQ
        </div>
        <!--End of Pop-up Content for Wego Vehicle location and waypoint-->
        
    </div> <!--End div "container">	-->



    /********************* Javascript APIs Scripts***********************/
/*
      <script src="./js/animateMapMarker.js"></script>

  </body>
</html>
*/